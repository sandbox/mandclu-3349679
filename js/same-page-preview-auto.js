(function (Drupal, once) {
  'use strict';

  Drupal.behaviors.samePagePreviewAuto = {
    attach: function (context) {
      once('samePagePreviewAuto', '.node-form input.form-element[data-drupal-selector="edit-title-0-value"]', context).forEach(function (element) {
        element.addEventListener("keyup", function () {
          titleRefresh(element);
        });
      });
      once('samePagePreviewAuto', '.node-form input.form-element:not([data-drupal-selector="edit-title-0-value"])', context).forEach(function (element) {
        element.addEventListener("blur", function () {
          forceRefresh();
        });
      });
      once('samePagePreviewAuto', '.node-form .ck-editor__editable_inline', context).forEach(function (element) {
        element.addEventListener("blur", function () {
          forceRefresh();
        });
      });
      async function forceRefresh() {
        const component = document.querySelector('[data-drupal-selector="edit-preview"]');
        if (component) {
          const active = document.activeElement;
          await component.dispatchEvent(new Event("click"));
          active.focus();
        }
      }

      function titleRefresh(element) {
        // If no preview iframe, nothing to do.
        const iframe = document.querySelector('iframe.preview');
        if (!iframe) {
          return;
        }
        let preview = (iframe.contentWindow || iframe.contentDocument);
        if (preview.document) {
          preview = preview.document;
        }
        let title = preview.querySelector('h1');
        if (title) {
          // Some themes nest the text inside a span.
          if (title.querySelector('span')) {
            title = title.querySelector('span');
          }
          title.textContent = element.value;
        }
        else {
          // If title element isn't created yet, refresh the preview instead.
          forceRefresh();
        }
      }
    }
  };
}(Drupal, once));
